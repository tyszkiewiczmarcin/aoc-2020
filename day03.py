import numpy as np

def count_trees(grid, r,l):
	x, y  = 0, 0
	width = len(grid[0])
	height = len(grid)
	trees  = 0

	while True:
		x, y = x+r, y+l

		if (grid[y][x%width] == '#'):
			trees += 1
		if y == height-1:
			return trees

if __name__ == '__main__':
	with open('inputs/inp3.txt') as f:
		grid = f.read().splitlines()

	print('p1:',count_trees(grid, 3,1))

	deltas = [(1,1),(3,1),(5,1),(7,1),(1,2)]
	counter = [count_trees(grid, r,l) for r, l in deltas]
	print('p2:',np.prod(counter))