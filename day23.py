def toDict(a):
    ptr = {}
    ptr[a[-1]] = a[0]
    for i in range(len(a)-1):
        ptr[a[i]] = a[i+1]
    return ptr

def makemoves(cups, n_moves):
    maxval = max(cups)
    curr = cups[0]
    ptr = toDict(cups)
    for _ in range(n_moves):
        # Select 3 cups after current
        c1 = ptr[curr]
        c2 = ptr[c1]
        c3 = ptr[c2]
        end = ptr[c3]
        ptr[curr] = end

        # Select destination cup
        dest = curr
        while True:
            if dest-1 > 0:
                dest -= 1
            else:
                dest = maxval
            if dest in [c1, c2, c3]:
                continue
            else:
                break

        # Put 3 cups immediately after 
        end = ptr[dest]
        ptr[dest] = c1
        ptr[c3] = end
        # Select next
        curr = ptr[curr]
    return ptr

def sol1(ptr):
    def gen(nxt):
        while ptr[nxt] != 1:
            nxt = ptr[nxt]
            yield nxt
            
    return int(''.join(str(val) for val in gen(1)))

def sol2(ptr):
    c1  = ptr[1]
    c2 = ptr[c1]
    return c1 * c2

cups = [9, 7, 4, 6, 1, 8, 3, 5, 2]
print(f'{sol1(makemoves(cups, n_moves=100))=}')

cups.extend(range(10, 1_000_001))
print(f'{sol2(makemoves(cups, n_moves=10_000_000))=}')