import numpy as np
from collections import Counter
from typing import List, Tuple
Tile = Tuple[int,int,int]
# https://www.redblobgames.com/grids/hexagons/

def parse(s: str) -> Tile:
    cube_coordinates = {
        'e' : [-1, 1, 0],
        'ne': [ 0, 1,-1],
        'se': [-1, 0, 1],
        'w' : [ 1,-1, 0],
        'nw': [ 1, 0,-1],
        'sw': [0, -1, 1]
    }
    i = 0
    vect = np.array([0, 0, 0])
    while i < len(s):
        if s[i:i+2] in cube_coordinates:
            vect += cube_coordinates[s[i:i+2]]; i += 2
        else:
            vect += cube_coordinates[s[i]]; i += 1
    return tuple(vect)


def main():
    with open('inputs/inp24.txt') as f:
        lines = f.read().splitlines()
    
    flipped = [parse(line) for line in lines]
    black   = [tile for tile, flips_count in Counter(flipped).items() if flips_count % 2 == 1]
    print(f'p1: {len(black)}')
    
    n = 100
    for i in range(n):
        black = day(black)
        print(f'{i+1}/{n} days\r', end='')
    print(f'\np2: {len(black)}')


def day(black: List[Tile]):
    def neighbours(pos: Tile) -> List[Tile]:
        x, y, z = pos
        return [(x - 1, y + 1, z    ),
                (x    , y + 1, z - 1),
                (x - 1, y    , z + 1),
                (x + 1, y - 1, z    ),
                (x + 1, y    , z - 1),
                (x    , y - 1, z + 1)]
        
    def is_black(tile: Tile) -> bool:
        return tile in black
    
    def concatenate(lists: List[List[Tile]]) -> List[Tile]:
        return [item for sublist in lists for item in sublist]
    
    #   Number of BLACK tiles adjacent to WHITE's 
    # = Number of WHITE duplicates form all BLACK tile's neighbours
    black_neighbours = Counter(concatenate([neighbours(tile) for tile in black]))
    
    black_new = []
    for tile, blacks_around in black_neighbours.items():
        if is_black(tile):
            if blacks_around == 0 or blacks_around > 2:
                continue
            else:
                black_new.append(tile)
        else:
            if blacks_around == 2:
                black_new.append(tile)
    return black_new
   
if __name__ == "__main__":
    main()