def parse_input(path, example=False):
    if example:
        return [9,2,6,3,1], [5,8,4,7,10]
    with open(path) as f:
        deck1, deck2 = f.read().replace('Player ','').replace('1:\n','').replace('2:\n','').split('\n\n')
    return [int(x) for x in deck1.split('\n')], [int(x) for x in deck2.split('\n')]

def winning_score(deck):
    return sum([(i+1) * v for i, v in enumerate(reversed(deck))])

def combat(d1, d2):
    while len(d1) != 0 and len(d2) != 0:
        hand1, hand2 = d1.pop(0), d2.pop(0)
        if hand1 > hand2:
            d1.extend([hand1,hand2])
        if hand1 < hand2:
            d2.extend([hand2,hand1])
            
    if len(d2) == 0:
        return (1, d1)
    return (2, d2)


def main():
    d1, d2 = parse_input('inputs/inp22.txt', example=True)
    
    p, d = combat(d1.copy(), d2.copy())
    print(f'Player {p} won the combat wih score:\n{winning_score(d)}')            # 291 <- fine
    p, d = recursive_combat(d1.copy(), d2.copy())
    print(f'Player {p} won the recursive combat with score:\n{winning_score(d)}') # 11960 <- wrong


def recursive_combat(d1, d2, game_num=1):
    rounds   = []
    previous = None
    while len(d1) != 0 and len(d2) != 0:
        print(f'-- Round {len(rounds)+1} (Game {game_num}) --')
        print("Player 1's deck: ", d1, " len: ", len(d1))
        print("Player 2's deck: ", d2, " len: ", len(d2))
        
        # TODO: Checking previous "card matches" propably has a bug
        if previous in rounds[:-1]:
            print(f'{previous} already occured in this game! Player 1 wins.')
            return (1, d1)
        rounds.append(previous)
        hand1, hand2 = d1.pop(0), d2.pop(0)
        current = ((hand1,d1), (hand2,d2))
        previous = current
        
        print("Player 1's hand: ", hand1)
        print("Player 2's hand: ", hand2)
        # If both players have at least as many cards remaining in their deck as the value of the card they just drew,
        # the winner of the round is determined by playing a new game of Recursive Combat
        winner = 0
        if len(d1) >= hand1 and len(d2) >= hand2:
            print('Playing a sub-game to determine the winner...')
            winner, _ = recursive_combat(d1[:hand1], d2[:hand2], game_num+1)
        
        # If no sub-game winner, higher card value wins the round
        if winner == 0:
            winner = 1 if hand1 > hand2 else 2
        if winner == 1:
            d1.extend([hand1,hand2])
        if winner == 2:
            d2.extend([hand2,hand1])
            
    if len(d2) == 0:
        return (1, d1)
    return (2, d2)


 
if __name__ == '__main__':
    main()