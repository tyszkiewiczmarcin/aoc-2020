import numpy as np
from numpy import sin, cos, radians
def parse_input(PATH):
	with open(PATH) as f:
		data = f.read().splitlines()
	return [(instr[0], int(instr[1:])) for instr in data]

def part1(data):
	def parse_angle(alpha):
		return int((alpha % 360)/90)

	pos    = {'E':0,'S':0,'W':0,'N':0}
	offset = 0

	for name, value in data:
		if name in pos.keys():
			pos[name] += value
		elif name == 'F':
			d = list(pos)[offset]
			pos[d] += value
		else:
			if name == 'R':
				offset += parse_angle(value)
			if name == 'L':
				offset += 4 - parse_angle(value)
			offset %= 4

	return abs(pos['W']-pos['E']) + abs(pos['N']-pos['S'])


def part2(data):
	def rotate(point, angle):
		angle = radians(angle)
		px, py = point

		qx = cos(angle) * px - sin(angle) * py
		qy = sin(angle) * px + cos(angle) * py
		return np.array([int(round(qx)), int(round(qy))])

	ship = np.array([0, 0])
	wp   = np.array([10,1])

	for name, val in data:
		if name == 'E':
			wp += [val, 0]
		if name == 'W':
			wp += [-val,0]
		if name == 'N':
			wp += [0, val]
		if name == 'S':
			wp += [0, -val]
		if name == 'L':
			wp = rotate(wp, val)
		if name == 'R':
			wp = rotate(wp,-val)
		if name == 'F':
			ship += val * wp
		print(name, val, ship, wp)
	return sum(abs(ship))



def main():
	data = parse_input('inputs/inp12.txt')
	print(part2(data))


if __name__ == '__main__':
	main()