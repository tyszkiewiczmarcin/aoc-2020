def transform(subject, loop_size):
    value = 1
    for _ in range(loop_size):
        value *= subject
        value %= 20201227
    return value

def secret_loop_size(subject, target_public_key):
    loop_size = 0
    value = 1
    while True:
        value *= subject
        value %= 20201227
        loop_size += 1
        if value == target_public_key:
            return loop_size
            
card_public_key, door_public_key = 11562782, 18108497
card_ls = secret_loop_size(7, card_public_key)
# door_ls = secret_loop_size(7, door_public_key)
print(transform(door_public_key, card_ls))