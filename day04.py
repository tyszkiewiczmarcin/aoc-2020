from matplotlib.colors import is_color_like
with open('inputs/inp4.txt') as f:
	passports = f.read().split('\n\n')

def has_expected_fields(passport):
	return all([field in passport for field in ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']])

def validate(passport):
	def required_height(h):
		if 'cm' not in h and 'in' not in h:
			return False
		v, m = int(h[:-2]), h[-2:]
		return (m=='cm' and 150 <= v <= 193) or (m=='in' and 59 <= v <= 76)

	REQ = {
		'byr': lambda x: 1920 <= int(x) <= 2002 and len(x)==4,
		'iyr': lambda x: 2010 <= int(x) <= 2020 and len(x)==4,
		'eyr': lambda x: 2020 <= int(x) <= 2030 and len(x)==4,
		'hgt': lambda x: required_height(x),
		'hcl': lambda x: is_color_like(x),
		'ecl': lambda x: x in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'],
		'pid': lambda x: len(x)==9,
		'cid': lambda x: True,
	}

	if not has_expected_fields(passport):
		return False

	passport = passport.replace('\n',' ').split(' ')
	passport = [tuple(field.split(':')) for field in passport]

	return all([REQ[field](val) for field, val in passport])
	
print('p1:',sum([has_expected_fields(p) for p in passports]))
print('p2:',sum(map(validate, passports)))

