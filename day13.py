def parse_input(path):
	with open(path) as f:
		departure = int(f.readline())
		tokens = f.readline().split(",")
	busses = [(bus_id, int(bus)) for bus_id, bus in enumerate(tokens) if bus != 'x']
	return departure, busses

def part1(departure, busses):
	waitingTime, bus = min([(bus - departure % bus, bus) for _, bus in busses])
	return waitingTime * bus

def part2(departure, busses):
	_, period = busses[0]
	t = 0
	for bus_id, bus in busses[1:]:
		offset = None
		while True:
			if (t + bus_id) % bus == 0:
				if offset is None:
					offset = t
				else:
					period = t - offset
					break

			t += period

	return offset

if __name__ == '__main__':
	departure, busses = parse_input('inputs/inp13.txt')
	print("p1:", part1(departure, busses))
	print("p2:", part2(departure, busses))

	