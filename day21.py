from collections import defaultdict
from typing import Dict, Set

def parse_input(path: str):
    output = []
    allergs_all = set()
    ingreds_all = set()
    with open(path) as f:
        foods = f.read().splitlines()
    
    for food in foods:
        ingred, allerg = food.strip(')').split(' (contains ')
        ingred = set(ingred.split(' '))
        allerg = set(allerg.split(', '))
        allergs_all.update(allerg)
        ingreds_all.update(ingred)
        output.append((ingred, allerg))
        
    return output, allergs_all, ingreds_all
    
def get_antigen_candidates(foods: Set[str], all_allergs: Set[str]):
    cands = defaultdict(set)
    for a in all_allergs:
        for ingred, alerg in foods:
            if a in alerg:
                if len(cands[a]) == 0:
                    cands[a] = ingred
                else:
                    cands[a] = cands[a].intersection(ingred)
    return cands

def match_allergens(cand: Dict[str, Set[str]]):
    matches = []
    while len(cand.values()) != 0:
        for a, c in cand.items():
            if len(c) == 1:
                matches.append((a,next(iter(c))))
                del cand[a]
                for target in cand.keys():
                    cand[target].difference_update(c)
                break
    return matches
    
def main():
    foods, all_alergs, all_ingreds = parse_input('inputs/inp21.txt')
    cands = get_antigen_candidates(foods, all_alergs)
    matches = match_allergens(cands)
    matched_ingreds = set([ingred for _, ingred in matches])
    safe = all_ingreds.difference(matched_ingreds)
    
    # 1) How many times do any of safe ingredients appear?
    occur = 0
    for ingred, _ in foods:
        occur += len(ingred.intersection(safe))
    print(f'p1: {occur}')
    
    # 2) Sort ingredients by allergen name alphabetically, and create `canonical dangerous ingredient list`.
    matches_sorted = sorted(matches, key=lambda e: e[0])
    print(f"p2: {''.join([ingred+',' for _, ingred in matches_sorted])[:-1]}")
            
if __name__ == "__main__":
    main()