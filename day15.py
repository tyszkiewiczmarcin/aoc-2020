from collections import defaultdict

num = [11,18,0,20,1,7,16]
occ = defaultdict(list)

for i, x in enumerate(a):
	occ[x].append(i)

while len(num) != 30_000_000:
	v = num[-1]
	if len(occ[v]) <= 1:
		num.append(0)
	else:
		d = occ[v][-1] - occ[v][-2]
		num.append(d)

	occ[num[-1]].append(len(num)-1)

print('p1:', a[2020-1])
print('p2:', a[-1])