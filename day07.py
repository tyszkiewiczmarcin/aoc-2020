from collections import defaultdict

with open('inputs/inp7.txt') as f:
	lines = f.read().replace(' bags','').replace(' bag','').replace('.','').splitlines()
	bags = defaultdict(list)
	for line in lines:
		color, content = line.split(' contain ')
		for item in content.split(', '):
			if item == 'no other':
				bags[color].append((0,'empty'))
			else:
				bags[color].append((int(item[0]), item[2:]))

def has_shiny(color):
	if color == 'shiny gold':
		return True
	return any(has_shiny(c) for _, c in bags[color])

def required_amount(color):
	return sum(n + n * required_amount(c) for n, c in bags[color])

print('p1:', sum(has_shiny(color) for color in list(bags)) - 1)
print('p2:', required_amount('shiny gold'))
