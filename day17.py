from __future__ import annotations

class Cube:
	def __init__(self, isActive):
		self.active = isActive
		self.waiting = False

	def switch(self):
		self.active = not self.active
		
	def update(self):
		if self.waiting:
			self.active = not self.active
			self.waiting = False

def parse_active(grid: List[str]) -> List[Tuple[int,int,int,int]]:
	return [(x,y,0,0) for y, line in enumerate(grid) for x, c in enumerate(line) if c == '#']
	
def init_space(d: int, n: int, active: List[Tuple[int,int,int,int]]) -> dict:
	return {(x,y,z,w) : Cube((x,y,z,w) in active) for x in range(-n,d+n+1) for y in range(-n,d+n+1) for z in range(-n,n+1) for w in range(-n,n+1)}

def make_step(d: int, c: int, cubes: dict) -> dict:
	def active_adjacent(p) -> int:
		x, y, z, w = p
		def active(dx,dy,dz,dw) -> bool:
			return (x+dx,y+dy,z+dz,w+dw) in cubes and cubes[(x+dx,y+dy,z+dz,w+dw)].active

		return sum([active(dx,dy,dz,dw) for dx in range(-1,2) for dy in range(-1,2) for dz in range(-1,2) for dw in range(-1,2) if not (dx == dy == dz == dw == 0)])

	scanSpace = [(x,y,z,w) for x in range(-c,d+c+1) for y in range(-c,d+c+1) for z in range(-c,c+1) for w in range(-c,c+1)]

	for poss in scanSpace:
		counter = active_adjacent(poss)
		isActive = cubes[poss].active
		if (isActive and counter not in [2,3]) or (not isActive and counter == 3):
			cubes[poss].waiting = True

	for poss in scanSpace:
		cubes[poss].update()

	return cubes

def count_active(cubes: dict) -> int:
	return sum([c.active for c in cubes.values()])

if __name__ == '__main__':
	with open('inputs/inp17.txt') as f:
		a = parse_active(f.read().splitlines())

	d, n  = len(a), 6
	cubes = init_space(d, n, a)

	for cycle in range(1,n+1):
		cubes = make_step(d, cycle, cubes)
		print(f'{cycle}/6 done')


	print('Active:', count_active(cubes))
	print('Active:', count_active(cubes))
