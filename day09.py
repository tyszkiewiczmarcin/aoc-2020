from __future__ import annotations
from itertools import combinations

with open('inputs/inp9.txt') as f:
	numbers =  [int(x) for x in f.read().splitlines()]

def possible_sums(arr: list[int]) -> list[int]:
	return [ a + b for a, b in combinations(arr,2) ]

def first_invalid(set_length: int) -> int:
	# invalid:    No 2 of N predecessors add up to this number
	# set_length: Number of preamble numbers
	for i in range(set_length,len(numbers)+1):
		if numbers[i] not in possible_sums(numbers[i-set_length:i]):
			return numbers[i]

def contiguous_sets(arr: list[int], set_length: int) -> list:
	# Creates patches of fixed size from a 1D list
	return [ arr[i-set_length:i] for i in range(set_length, len(arr)+1) ]

def encryption_weakness(invalid: int) -> list[int]:
	for set_length in range(2, len(numbers)):
		for cont_set in contiguous_sets(numbers,set_length):
			if sum(cont_set) == invalid:
				return min(cont_set) + max(cont_set)


def main() -> None:
	invalid = first_invalid(set_length=25)
	print('p1:',invalid)
	print('p2:',encryption_weakness(invalid))

if __name__ == '__main__':
	main()
	
