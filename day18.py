class a:
  def __init__(self,value):
    self.value = value
  def __mul__(self,other):
    return a(self.value * other.value)
  def __pow__(self,other):
    return a(self.value + other.value)
  def __matmul__(self,other):
    return a(self.value + other.value)
  def __repr__(self):
    return f'a({self.value})'

def solve(eq, sub):
	eq = eq.replace('(','( ').replace(')',' )').split(' ')
	parsed_eq = ''.join([f'a({c})' if c.isnumeric() else c for c in eq]).replace('+',sub)
	return eval(parsed_eq).value

if __name__ == '__main__':
	with open('inputs/inp18.txt') as f:
		lines = f.read().splitlines()
	print('p1:',sum(solve(line, '@') for line in lines))
	print('p2:',sum(solve(line,'**') for line in lines))