import numpy as np

def parse_rule(line):
	_, ranges = line.split(': ')
	r1, r2 = ranges.split(' or ')
	x1, y1 = r1.split('-')
	x2, y2 = r2.split('-')
	return (int(x1),int(y1)), (int(x2),int(y2))

def dont_match(v, rule):
	r1, r2 = rule
	if not (r1[0] <= v <= r1[1]) and not (r2[0] <= v <= r2[1]):
		return True
	return False

def sum_invalid(rules, tickets):
	ticket_vals = np.concatenate(tickets, axis=None)
	invalid_vals = filter(lambda v: all([dont_match(v,r) for r in rules]), ticket_vals)
	return sum(invalid_vals)

def classify(mm, n):
	ruleDict = {}
	while len(list(ruleDict)) < n:
		for r, t in enumerate(mm):
			if sum(t) == 1:
				c = np.where(t == True)[0][0]
				ruleDict[c] = r
				mm[:,c] = False
				break
	
	return ruleDict

def get_matchmatrix(rules, tickets):
	def match(v, rule):
		r1, r2 = rule
		return (r1[0] <= v <= r1[1] or r2[0] <= v <= r2[1])

	def invalid(t):
		return any(map(lambda v: all([dont_match(v,r) for r in rules]), t))

	# Filter tickets not matching any
	tickets = np.array(list(filter(lambda t: not invalid(t), tickets)))

	n = tickets.shape[1]
	matchmatrix = [[] for _ in range(n)]

	for i in range(n):
		col = tickets[:,i]
		for rule in rules:
			success = all([match(v, rule) for v in col])
			matchmatrix[i].append(success)

	return np.array(matchmatrix)

	
if __name__ == '__main__':
	with open('inputs/inp16.txt') as f:
		rules, mine, tickets = f.read().split('\n\n')

	rules   = [parse_rule(line) for line in rules.splitlines()]
	mine    = [int(x) for x in (mine.splitlines()[1]).split(',')]
	tickets = [[int(v) for v in t.split(',')] for t in tickets.splitlines()[1:]]
	
	mm = get_matchmatrix(rules, tickets)
	ruleDict = classify(mm, n=len(rules))
	res = np.prod([mine[ruleDict[i]] for i in range(6)])

	print('p1:', sum_invalid(rules, tickets))
	print('p2:', res)
