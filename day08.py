from __future__ import annotations

class BootCode:
	_program: tuple[str,int]
	_visited_instructions: set[int]
	_accumulator: int
	_position: int
	
	def __init__(self, PATH: str) -> None:
		self._program = self._parse_instructions(PATH)

	def _restart(self) -> None:
		self._position = 0
		self._accumulator = 0
		self._visited_instructions = set()
		
	def _parse_instructions(self, PATH: str) -> list[tuple[str,int]]:
		with open(PATH) as f:
			instructions = f.read().splitlines()

		return [instr.split(' ') for instr in instructions]

	def run(self) -> tuple[int,bool]:
		self._restart()

		normal_exit = False

		while (self._position not in self._visited_instructions):
			self._visited_instructions.add(self._position)

			try:
				instr = self._program[self._position]
			except IndexError:
				normal_exit = True
				break

			name, value = instr
			if name == 'jmp':
				self._position += int(value)
			if name == 'nop':
				self._position += 1
			if name == 'acc':
				self._accumulator += int(value)
				self._position += 1

		return self._accumulator, normal_exit

	def doctor(self) -> None:
		for i, instr in enumerate(self._program):
			name, value = instr

			if name == 'nop':
				self._program[i] = ('jmp',value)
			elif name == 'jmp':
				self._program[i] = ('nop',value)
			else:
				continue
				
			_, normal_exit = self.run()

			if normal_exit:
				return
			else:
				self._program[i] = instr


def main() -> None:
	bc = BootCode('inputs/inp8.txt')
	print('p1:', bc.run())
	bc.doctor()
	print('p2:', bc.run())

if __name__ == '__main__':
	main()
