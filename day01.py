def part1(a):
	for x in a:
		for y in a:
			if x != y and x+y == 2020:
				return x*y

def part2(a):
	for x in a:
		for y in a:
			for z in a:
				if x not in [y,z] and x+y+z == 2020:
					return x*y*z


if __name__ == '__main__':
	with open('inputs/inp1.txt') as f:
		a = f.read().splitlines()
	a = list(map(int, a))

	print('p1:',part1(a))
	print('p2:',part2(a))

