def part1(groups: list) -> int:	
	unique_answers = (set(answers.replace('\n','')) for answers in groups)
	return sum(len(answer) for answer in unique_answers)

def part2(groups: list) -> int:
	def all_yes(persons_answers: list) -> int:
		common_answers = set.intersection(*(set(answer) for answer in persons_answers))
		return len(common_answers)

	return sum(map(all_yes,(answers.split('\n') for answers in groups)))

if __name__ == "__main__":
	groups = open('inputs/inp6.txt').read().split('\n\n')
	print(f'p1: {part1(groups)}\np2: {part2(groups)}')

