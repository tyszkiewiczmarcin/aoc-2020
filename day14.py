from itertools import product
BITS_AMOUNT = 36
"""
def apply_mask1(mask: str, value: str) -> str:
	return ''.join([x if mask[i] in ['1','0'] else value[i] for i, x in enumerate(mask)])

def part1(prog: list) -> int:
	memo = {}
	for line in prog:
		if 'mask' in line:
			mask = line[7:]
		
		if 'mem' in line:
			_, addr, _, _, value = line.replace('[',' ').replace(']',' ').split(' ')

			bin_value = bin(int(value)).replace('0b','')
			bin_value = (BITS_AMOUNT-len(bin_value))*'0' + bin_value
			
			memo[addr] = apply_mask1(mask, bin_value)

	return sum([int(s, 2) for s in memo.values()])
"""

def apply_mask2(mask: str, addr: str) -> str:
	return ''.join([x if mask[i] in ['X','1'] else addr[i] for i, x in enumerate(mask)])

def part2(prog: list):
	memo = {}
	for line in prog:
		if 'mask' in line:
			mask = line[7:]

		if 'mem' in line:
			_, addr, _, _, value = line.replace('[',' ').replace(']',' ').split(' ')

			bin_addr = bin(int(addr)).replace('0b','')
			bin_addr = (BITS_AMOUNT-len(bin_addr))*'0' + bin_addr

			floating = apply_mask2(mask, bin_addr)

			n = floating.count('X')
			for tweaks in product('10',repeat=n):
				addr = floating

				for x in tweaks:
					addr = addr.replace('X', str(x), 1)

				memo[addr] = int(value)
			
	return sum(memo.values())


if __name__ == '__main__':
	with open('inputs/inp14.txt') as f:
		prog = f.read().splitlines()

	print('p1:', part1(prog))
	print('p2:', part2(prog))

