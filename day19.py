from itertools import product
# Cartesian product of input iterables. Equivalent to nested for-loops
# product([1,2],[3,4]) = generator( (1, 3), (1, 4), (2, 3), (2, 4) )

def main():
	with open('inputs/inp19.txt') as f:
		rules, msg = f.read().split('\n\n')
	rules, msg = rules.splitlines(), msg.splitlines()
	rules = [rule.split(': ') for rule in rules]
	rules = {int(i): v for i, v in rules}

	def solve(i, d=0):
		if d > 10: # 11 <- Min depth working for p1, Also min depth at which 42 and 31 have fixed lengths
			return
		if '"' in rules[i]:
			yield rules[i].strip('"')
		else:
			for sub in rules[i].split(" | "):
				for p in product( *[solve(int(s), d+1) for s in sub.split()] ):
					yield ''.join(p)

	valid = set(solve(0))
	print('p1:', sum(s in valid for s in msg))

	rules[8]  = "42 | 42 8"
	rules[11] = "42 31 | 42 11 31"

	first = set(solve(42))
	print(first)
	second = set(solve(31))
	print(second)

if __name__ == '__main__':
	# print(list(product([1,2],[3,4])))
	main()