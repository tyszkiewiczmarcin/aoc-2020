from collections import defaultdict
from numpy import prod

def extract_borders(tile):
    # Returns all borders and their reversed order
    u, d, l, r = tile[0], tile[-1], ''.join([row[0] for row in tile]), ''.join([row[-1] for row in tile])
    return set([u, u[::-1], d, d[::-1], l, l[::-1], r, r[::-1]])

def parse_input(path):
    output = []
    with open(path) as f:
        tiles = f.read().split('\n\n')
    for tile in tiles:
        idx, *content = tile.split('\n')
        idx = int(idx.strip('Tile :'))
        output.append((idx,extract_borders(content)))
    return output

def find_adj(tiles):
    adj = defaultdict(set)
    for i, b1 in tiles:
        for j, b2 in tiles:
            if i == j:
                continue
            if len(b1.intersection(b2)) != 0:
                adj[i].add(j)
    return adj
        
def main():
    tiles = parse_input('inputs/inp20.txt')
    adjDict = find_adj(tiles)
    corner_mult = prod([i for i, neighbours in adjDict.items() if len(neighbours)==2])
    print(corner_mult)
    
if __name__ == '__main__':
    main()