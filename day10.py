from collections import Counter
from numpy import prod

with open('inputs/inp10.txt') as f:
	lines = [int(x) for x in f.read().splitlines()]
	lines  = sorted(lines + [0,max(lines)+3])

def differences(arr):
	return [arr[i+1]-arr[i] for i in range(len(arr)-1)]
	
def occurences(n, arr):
	occur = []
	counter = 0
	for e in arr:
		if e == n:
			counter += 1
		if e == 3:
			if counter != 0:
				occur.append(counter)
			counter = 0
	return occur

class Graph: 
  
	def __init__(self, V): 
		self.V = V  
		self.adj = [[] for i in range(V)] 
	  
	def addEdge(self, u, v): 
		self.adj[u].append(v) 
	  
	def countPaths(self, s, d): 
		visited = [False] * self.V 
	  
		pathCount = [0]  
		self.countPathsUtil(s, d, visited, pathCount)  
		return pathCount[0] 

	def countPathsUtil(self, u, d, visited, pathCount): 
		visited[u] = True
	  
		if (u == d): 
			pathCount[0] += 1
		else: 
			# Recur for all the vertices  
			# adjacent to current vertex 
			i = 0
			while i < len(self.adj[u]): 
				if (not visited[self.adj[u][i]]):  
					self.countPathsUtil(self.adj[u][i], d, visited, pathCount) 
				i += 1
	  
		visited[u] = False
  
def count_paths(one_cnt):
	if one_cnt == 1:
		return 1
	v = one_cnt + 1
	g = Graph(v)
	for i in range(v-3):
		g.addEdge(i,i+1)
		g.addEdge(i,i+2)
		g.addEdge(i,i+3)
	g.addEdge(v-3,v-2)
	g.addEdge(v-3,v-1)
	g.addEdge(v-2,v-1)
	return g.countPaths(0, v-1)

diff = differences(lines)
counted = Counter(diff)
print('p1:',counted[1] * counted[3])

one_counts = occurences(1, diff)
print('p2:',prod([count_paths(cnt) for cnt in one_counts]))