from collections import Counter

def step1(grid):
	updated = 0
	new = [['.' for _ in row] for row in grid]
	for i, row in enumerate(grid):
		for j, c in enumerate(row):
			ct = Counter(grid[i + dx][j + dy] for dx in range(-1, 2) for dy in range(-1, 2) if 0 <= i + dx < len(grid) and 0 <= j + dy < len(grid[i]) and (dx, dy) != (0, 0))
			if   c == 'L' and ct['#'] == 0:
				new[i][j] = '#'
				updated += 1
			elif c == '#' and ct['#'] >= 4:
				new[i][j] = 'L'
				updated += 1
			else:
				new[i][j] = grid[i][j]
				
	return new, updated

def step2(grid):
	new = [['.' for _ in row] for row in grid]
	h, w = len(grid), len(grid[0])
	updated = 0

	for i, row in enumerate(grid):
		for j, c in enumerate(row):
			occupied = 0
			for dx in range(-1, 2):
				for dy in range(-1, 2):
					if (dx, dy) == (0, 0):
						continue

					x = j + dx
					y = i + dy
					while 0 <= x < w and 0 <= y < h and grid[y][x] == '.':
						x += dx
						y += dy

					if 0 <= x < w and 0 <= y < h:
						occupied += grid[y][x] == '#'

			if   c == 'L' and occupied == 0:
				new[i][j] = '#'
				updated += 1
			elif c == '#' and occupied >= 5:
				new[i][j] = 'L'
				updated += 1
			else:
				new[i][j] = grid[i][j]

	return new, updated

def main():
	with open('inputs/inp11.txt') as f:
		grid = f.read().splitlines()

	updated = None
	while updated != 0:
		new, updated = step2(grid)
		print('updated cells:', updated)
		grid = new

	print(sum(Counter(line)['#'] for line in grid))


if __name__ == "__main__":
	main()