from collections import Counter
	
def parse_lines(ls):
	def parse(l):
		interval, c, passwd = l.split(' ')
		l, u = interval.split('-')
		l, u = int(l), int(u)
		c = c.strip(':')
		return passwd, c, l, u
	return [parse(line) for line in ls]

def part1(data):
	return sum([(l <= Counter(passwd)[c] <= u) for passwd, c, l, u in data])

def part2(data):
	return sum([((passwd[l-1] == c) ^ (passwd[u-1] == c)) for passwd, c, l, u in data])

if __name__ == '__main__':
	with open('inputs/inp2.txt') as f:
		lines = f.read().splitlines()
	data = parse_lines(lines)
	print('p1:',part1(data))
	print('p2:',part2(data))
