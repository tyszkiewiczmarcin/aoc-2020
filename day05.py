from math import ceil
def decode(strcode):
	def binary_search(strcode, markers):
		a, b = 0, 2**len(strcode)-1
		lower, upper = markers
		for c in strcode:
			mid = ceil((b - a) / 2)
			if c == lower:
				b -= mid
			if c == upper:
				a += mid
		return a
	return binary_search(strcode[:7],('F','B')), binary_search(strcode[7:],('L','R'))

def seat_id(strcode):
	r, c = decode(strcode)
	return r * 8 + c

seats = open('inputs/inp5.txt').read().splitlines()
seat_ids = sorted(map(seat_id,seats))
print('p1:',max(seat_ids))
for i in range(len(seat_ids)-1):
	if seat_ids[i+1] - seat_ids[i] != 1:
		print('p2:',seat_ids[i]+1)
